import './App.css'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Header from "./components/Index/Header/Header";

import Index from './components/Index/Index'
import Favorites from './components/Pages/Favorites/Favorites'
import BasketPage from './components/Pages/Basket/Basket';

import { useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch()

  return (
    <>
        
    <Router>
    <Header/>
      <Routes>
        <Route path="/" element={<Index/> } />
        <Route path="/favorites" element={<Favorites/>} />
        <Route path="/basket" element={<BasketPage/>}/>
      </Routes>
    </Router> */
    </>
  );
}

export default App
