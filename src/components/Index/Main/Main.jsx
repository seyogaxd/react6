import ShopList from "./ShopList/ShopList";
import { useViewMode } from "../../../Context/Context";

export default function Main(props)
{
    const { viewMode, toggleViewMode } = useViewMode();

    return(
        <div className="main__container">
            <h1>Our Products</h1>
            <button onClick={toggleViewMode}>{`Change View Mode on ${viewMode === 'table' ? 'cards' : 'table'}`}</button>
            <div className={`main__container-items`}>
                <div className={`main__items-list ${viewMode === 'table' ? 'card-view' : 'table-view'}`}>
                <ShopList/>
            </div>
            </div>
            
        </div>
    )
}