import FavoriteIcon from "../../../../assets/icons/favorite-icon.svg"
import FavoriteIconPressed from "../../../../assets/icons/favorite-icon-pressed.svg"
import Button from "../../../Button/Button"

import { addToFavorite } from "../../../../Redux/favorite.slice/favorite.slice"
import { useDispatch, useSelector } from "react-redux";
import { openModal, openImageModal } from "../../../../Redux/modal.slice/modal.slice";

export default function ShopItem(props)
{
    const dispatch = useDispatch();

    const {
        id,
        image,
        name,
        price,
        isBasket
    } = props;

    const shopedItems = useSelector((state) => state.purchase.shopedItems);
    const favoriteItems = useSelector((state) => state.favorite.favoriteItems);

    const isBoughted = shopedItems.some((item) => item.id === id);
    const isFavorite = favoriteItems.some((item) => item.id === id);
    return(
        <div className="main__item">
            <div className="main__item-title">
            <p className="main__item-name">{name}</p>
            {isBasket&&(
                <span className="main__item-delete">
                <Button 
                classNames={"main__item-delete"}
                onClick={() => dispatch(openImageModal({itemId: id}))}
                >
                    &times;
                </Button>
                </span>
            )}
            
            </div>
            <div className="main__item-image"><img src={image} alt="TestIcon" /></div>
            <div className="main__item-buttons">
            {!isFavorite&&(
                <Button
            type={'button'}
            classNames={'main__item-favorite-button'}
            onClick={() => {dispatch(addToFavorite(id));}}
            >
                <img src={FavoriteIcon} alt="Favorite-icon" />
            </Button>
            )}
            {isFavorite&&(
                <Button
                type={'button'}
                classNames={'main__item-favorite-button'}
                onClick={() => dispatch(addToFavorite(id))}
                >
                    <img src={FavoriteIconPressed} alt="Favorite-icon" />
                </Button>
            )}
            <span className="main__item-price">{price}</span>
            {!isBoughted&&(
                <Button
                type={'button'} 
                classNames={'main__item-button'} 
                onClick={() => {
                    dispatch(openModal({ itemId: id }));
                    //console.log(shopedItems);
                }}
            >
            Buy
            </Button>
            )}
            {isBoughted&&(
                <div className="main__item-button-boughted">Purchased</div>
            )
            }
            </div>
        </div>
    )
}
