import { render, fireEvent, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import Button from "./Button"

describe("Button", () => {
    test("renders with correct classnames", () => {
      const { container } = render(<Button classNames="my-button" />);
      const button = container.querySelector("button");
      expect(button).toHaveClass("my-button");
    });
  
    test("calls onClick function when clicked", () => {
      const handleClick = jest.fn();
      const { getByText } = render(
        <Button onClick={handleClick}>Click me</Button>
      );
      const button = getByText("Click me");
      fireEvent.click(button);
      expect(handleClick).toHaveBeenCalled();
    });
  });