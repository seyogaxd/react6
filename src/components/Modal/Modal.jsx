import './Styles/ModalStyles.scss'

export default function Modal({children})
{
    return(
        <div className="main__modal">
            {children}
        </div>
    )
}