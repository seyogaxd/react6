import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";

import { deleteFromShoped } from "../../../Redux/purchase.slice/purchase.slise";
import { useDispatch, useSelector } from "react-redux";
import { closeImageModal } from "../../../Redux/modal.slice/modal.slice";
import items from "../../../../public/items.json"

export default function ModalImage()
{
    const dispatch = useDispatch();
    const closeWindow = () =>{
        dispatch(closeImageModal());
    }

    const itemId = useSelector(state => state.modal.modalItemId);
    const selectedItem = items.find(item => item.id === itemId);
    return(
        <ModalWrapper>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow}></ModalClose>
                </ModalHeader>
                <ModalBody>
                    <img src={selectedItem.image} alt={selectedItem.name ? selectedItem.name : 'picture'} className={"main__modal-image"}/>
                    <h2>{"Product Delete!"}</h2>
                    <p>{`By clicking the “Yes, Delete” button, "${selectedItem.name}" will be deleted.`}</p>
                </ModalBody>
                <ModalFooter
                firstText={"NO, CANCEL"}
                secondaryText={"YES, DELETE"}
                firstClick={closeWindow}
                secondaryClick={() => {
                    dispatch(deleteFromShoped(itemId));
                    closeWindow();
                }}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}