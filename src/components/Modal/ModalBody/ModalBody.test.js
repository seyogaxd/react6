import React from 'react';
import TestRenderer from 'react-test-renderer';
import ModalBody from './ModalBody';

test('ModalBody component matches snapshot', () => {
  const component = TestRenderer.create(
    <ModalBody>
      <div>Child component</div>
    </ModalBody>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
