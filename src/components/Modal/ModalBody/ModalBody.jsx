import '../Styles/ModalStyles.scss'

export default function ModalBody({ children })
{
    return(
        <div className="main__modal-body">
            {children}
        </div>
    )
}