import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import Button from '../../../Button/Button';

import { NumericFormat } from 'react-number-format';
import { useDispatch, useSelector } from "react-redux";
import { openCheckoutModal, openImageModal } from "../../../../Redux/modal.slice/modal.slice";
import { clearShoped } from '../../../../Redux/purchase.slice/purchase.slise';
const CheckoutForm = () => {
  const dispatch = useDispatch();
  const submitOnClick = (value) => {
    const userCartInfo = JSON.parse(localStorage.getItem("ShopItems"));
    console.log(value, userCartInfo);
    //dispatch(openCheckoutModal());
    dispatch(clearShoped());
  };
  return (
    
    <div>
      <Formik
        initialValues={{
          firstName: '',
          lastName: '',
          age: '',
          address: '',
          phone: ''
        }}
        validationSchema={Yup.object({
          firstName: Yup.string()
            .required('Required'),
          lastName: Yup.string().required('Required'),
          age: Yup.number()
            .required('Required')
            .positive('Age must be a positive number')
            .integer('Age must be an integer'),
          address: Yup.string().required('Required'),
          phone: Yup.number()
            .required('Required')
        })}
        onSubmit={(values) => submitOnClick(values)}
      >
        <Form>
          <div>
            <label htmlFor="firstName">First Name</label>
            <Field type="text" name="firstName" />
            <ErrorMessage name="firstName" component="div" />
          </div>

          <div>
            <label htmlFor="lastName">Last Name</label>
            <Field type="text" name="lastName" />
            <ErrorMessage name="lastName" component="div" />
          </div>

          <div>
            <label htmlFor="age">Age</label>
            <Field type="text" name="age" />
            <ErrorMessage name="age" component="div" />
          </div>

          <div>
            <label htmlFor="address">Address</label>
            <Field type="text" name="address" />
            <ErrorMessage name="address" component="div" />
          </div>

          <div className='main__checkout-last-option'>
            <label htmlFor="phone">Phone</label>
            <Field
              as={NumericFormat}
              format="(###) ###-##-##"
              mask="_"
              type="tel"
              name="phone"
            />
            <ErrorMessage name="phone" component="div" />
          </div>

          <button type="submit">Checkout</button>
          {/* <Button 
          classNames={"main__checkout-button"}
          onClick={() => dispatch(openCheckoutModal())}
          >Checkout</Button> */}
        </Form>
      </Formik>
    </div>
  );
};

export default CheckoutForm;
