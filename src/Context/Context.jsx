import React, { createContext, useState, useContext } from 'react';

const ViewModeContext = createContext();

export const useViewMode = () => {
  return useContext(ViewModeContext);
};

export const ViewModeProvider = ({ children }) => {
  const [viewMode, setViewMode] = useState('cards');

  const toggleViewMode = () => {
    setViewMode((prevMode) => (prevMode === 'cards' ? 'table' : 'cards'));
  };

  return (
    <ViewModeContext.Provider value={{ viewMode, toggleViewMode }}>
      {children}
    </ViewModeContext.Provider>
  );
};
