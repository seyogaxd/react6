import purchaseReducer, { addToShoped, deleteFromShoped, clearShoped } from './purchase.slise';

describe('purchase reducer', () => {
  test('should return the initial state', () => {
    expect(purchaseReducer(undefined, {})).toEqual({
      shopedItems: [],
    });
  });

  test('should handle addToShoped', () => {
    const initialState = {
      shopedItems: [],
    };
    const itemToAdd = { 
        id: 1, 
        name:"red T-shirt", 
        price: 1550, 
        color:"red", 
        article: 61242, 
        image:"./images/red-tshirt.png", 
        };
    const nextState = purchaseReducer(initialState, addToShoped(itemToAdd.id));
    expect(nextState.shopedItems[0]).toEqual(expect.objectContaining(itemToAdd));
  });

  test('should handle deleteFromShoped', () => {
    const initialState = {
      shopedItems: [{ id: 1, name: 'Item 1', price: 10 }],
    };
    const nextState = purchaseReducer(initialState, deleteFromShoped(1));
    expect(nextState.shopedItems).toHaveLength(0);
  });

  test('should handle clearShoped', () => {
    const initialState = {
      shopedItems: [{ id: 1, name: 'Item 1', price: 10 }],
    };
    const nextState = purchaseReducer(initialState, clearShoped());
    expect(nextState.shopedItems).toHaveLength(0);
  });
});
