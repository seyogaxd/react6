import { configureStore } from '@reduxjs/toolkit';  

import dataSlice from './data.slice/data.slice';
import modalSlice from './modal.slice/modal.slice';
import favoriteSlice from './favorite.slice/favorite.slice';
import purchaseSlise from './purchase.slice/purchase.slise';

export const store = configureStore({
    reducer: {
        data: dataSlice,
        modal: modalSlice,
        favorite: favoriteSlice,
        purchase: purchaseSlise,
    },
});

export default store;