import { createSlice } from '@reduxjs/toolkit';

export const modalSlice = createSlice({
  name: 'modal',
  initialState: {
    isModalOpen: false,
    isImageModalOpen: false,
    isCheckoutModalOpen: false,
    modalItemId: null,
  },
  reducers: {
    openModal: (state, action) => {
      state.isModalOpen = true;
      state.modalItemId = action.payload.itemId;
    },
    openImageModal: (state, action) => {
      state.isImageModalOpen = true;
      state.modalItemId = action.payload.itemId;
    },
    openCheckoutModal: (state) => {
      state.isCheckoutModalOpen = true;
      console.log("Open");
    },
    closeModal: (state) => {
      state.isModalOpen = false;
    },
    closeImageModal: (state) => {
      state.isImageModalOpen = false;
    },
    closeCheckoutModal: (state) => {
      state.isCheckoutModalOpen = false;
    },
  },
});

export const { openModal, openImageModal, openCheckoutModal, closeModal, closeImageModal, closeCheckoutModal } = modalSlice.actions;

export default modalSlice.reducer;  