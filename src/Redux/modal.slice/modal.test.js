import modalReducer, { openModal, openImageModal, openCheckoutModal, closeModal, closeImageModal, closeCheckoutModal } from './modal.slice';

describe('modal reducer', () => {
  it('should handle openModal', () => {
    const initialState = {
      isModalOpen: false,
      modalItemId: null,
    };
    const nextState = modalReducer(initialState, openModal({ itemId: 123 }));
    expect(nextState.isModalOpen).toEqual(true);
    expect(nextState.modalItemId).toEqual(123);
  });

  it('should handle openImageModal', () => {
    const initialState = {
      isImageModalOpen: false,
      modalItemId: null,
    };
    const nextState = modalReducer(initialState, openImageModal({ itemId: 456 }));
    expect(nextState.isImageModalOpen).toEqual(true);
    expect(nextState.modalItemId).toEqual(456);
  });

  it('should handle openCheckoutModal', () => {
    const initialState = {
      isCheckoutModalOpen: false,
    };
    const nextState = modalReducer(initialState, openCheckoutModal());
    expect(nextState.isCheckoutModalOpen).toEqual(true);
  });

  it('should handle closeModal', () => {
    const initialState = {
      isModalOpen: true,
    };
    const nextState = modalReducer(initialState, closeModal());
    expect(nextState.isModalOpen).toEqual(false);
  });

  it('should handle closeImageModal', () => {
    const initialState = {
      isImageModalOpen: true,
    };
    const nextState = modalReducer(initialState, closeImageModal());
    expect(nextState.isImageModalOpen).toEqual(false);
  });

  it('should handle closeCheckoutModal', () => {
    const initialState = {
      isCheckoutModalOpen: true,
    };
    const nextState = modalReducer(initialState, closeCheckoutModal());
    expect(nextState.isCheckoutModalOpen).toEqual(false);
  });
});
